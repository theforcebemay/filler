# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile2                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/28 15:08:53 by bsemchuk          #+#    #+#              #
#    Updated: 2017/06/28 15:23:47 by bsemchuk         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = filler

SRC = src/allocation.c src/contact.c src/decide.c src/distance.c src/find_player.c src/game_info.c src/howto_set.c\
	  src/matrix.c src/readline.c

OB = $(SRC:.c=.o)

FLAGS = -g -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OB)
	make -C libft/
	gcc $(FLAGS) -o $(NAME) $(OB) libft/libft.a
%.o:%.c
	gcc $(FLAGS) -c -o $@ $<
clean:
	rm -f $(OB)
	make clean -C libft/
fclean: clean
	make fclean -C libft/
	rm -f $(NAME)
re: clean all
