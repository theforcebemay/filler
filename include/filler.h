/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 11:37:25 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 19:54:05 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# include "../libft/libft.h"
# define RESET2(a,b) a = 0, b = 0
# define RESET3(a,b,c) a = 0, b = 0, c = 0

typedef struct	s_player
{
	int			row;
	int			col;
	int			p;
	char		c;
}				t_player;

typedef struct	s_board
{
	int			row;
	int			col;
	char		**board;
}				t_board;

typedef struct	s_piece
{
	int			row;
	int			col;
	char		**piece;
}				t_piece;

int				read_and_detect_loop(void);
int				detect_line(char *line_content);

char			**allomatrix(int row, int col);
int				printmatrix(char **matrix);
int				makevismatrix(char **matrix, int row, int col);
int				deallomatrix(char **mx, int row);
int				test_matrix(int	row, int col);

int				deallo_global(char *line_content);
int				p_one_or_p_two(char *line_content);
int				board_width(char *line_content);
int				piece_width(char *line_content);
int				piece_into(char *line_content);
int				board_into(char *line_content);
int				can_i_be_placed(int row, int col);
int				naive_force(void);
int				find_player(t_player *player, char c);
int				distance_between(int x1, int y1, int x2, int y2);
int				solution(void);
int				where_to_set(int row, int col);
int				are_there_any(int row, int col);
int				what_about_contact(char c);
int				feedback(int i, int j);

extern t_player	g_me;
extern t_player	g_enemy;
extern t_board	g_board;
extern t_piece	g_piece;
extern int		g_best;
extern int		g_contact;

#endif
