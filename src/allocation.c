/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 11:29:51 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 16:36:14 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

/*
**	Fetching col and row (i, j) we get from stdin
**	and allocating memory needed into g_vars
*/

int		piece_width(char *line_content)
{
	if (g_piece.piece)
		deallomatrix(g_piece.piece, g_piece.row);
	while (!ft_isdigit(line_content[0]) && line_content[0])
		line_content++;
	g_piece.row = ft_atoi(line_content);
	while (ft_isdigit(line_content[0]) && line_content[0])
		line_content++;
	g_piece.col = ft_atoi(line_content);
	g_piece.piece = allomatrix(g_piece.row, g_piece.col);
	return (EXIT_SUCCESS);
}

int		board_width(char *line_content)
{
	if (g_board.board)
		deallomatrix(g_board.board, g_board.row);
	while (!ft_isdigit(line_content[0]) && line_content[0])
		line_content++;
	g_board.row = ft_atoi(line_content);
	while (ft_isdigit(line_content[0]) && line_content[0])
		line_content++;
	g_board.col = ft_atoi(line_content);
	g_board.board = allomatrix(g_board.row, g_board.col);
	return (EXIT_SUCCESS);
}
