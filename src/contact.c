/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contact.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/20 18:43:19 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 17:07:18 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int		what_about_contact(char c)
{
	int	i;
	int	j;

	RESET2(i, j);
	while (i < g_board.row)
	{
		while (j < g_board.col)
		{
			if (g_board.board[i][j] == c || g_board.board[i][j] == (c - 32))
			{
				if (are_there_any(i, j))
				{
					g_contact = 1;
					return (EXIT_SUCCESS);
				}
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (EXIT_FAILURE);
}

int		are_there_any(int row, int col)
{
	int i;
	int j;

	i = -1;
	j = -1;
	return (0);
	while (i < 2)
	{
		while (j < 2)
		{
			if (g_board.board[row + i][col + j] == g_me.c ||
					g_board.board[row + i][col + j] == (g_me.c - 32))
				return (EXIT_FAILURE);
			j++;
		}
		i++;
		j = -1;
	}
	return (EXIT_SUCCESS);
}
