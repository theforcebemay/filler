/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decide.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 14:49:28 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/28 16:27:33 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

/*
**	Place where computation begins
**	and goes & goes
*/

int		g_best;
int		g_contact = 0;

int		naive_force(void)
{
	int	i;
	int	j;

	RESET2(i, j);
	g_best = g_board.row * g_board.col;
	while ((i + g_piece.row) < g_board.row)
	{
		while ((j + g_piece.col) < g_board.col)
		{
			if (!can_i_be_placed(i, j))
				where_to_set(i, j);
			j++;
		}
		j = 0;
		i++;
	}
	feedback(g_me.row, g_me.col);
	return (EXIT_SUCCESS);
}

int		where_to_set(int row, int col)
{
	int			i;
	int			j;
	int			new;

	new = g_best;
	RESET2(i, j);
	while (i < g_piece.row)
	{
		while (j < g_piece.col)
		{
			if (g_piece.piece[i][j] == '*')
				new = distance_between(row + i, col + j,
						g_enemy.row, g_enemy.col);
			if (new < g_best)
			{
				g_best = new;
				g_me.row = row;
				g_me.col = col;
			}
			j++;
		}
		i++;
		j = 0;
	}
	return (EXIT_SUCCESS);
}

/*
**	Problem soling starts here;
*/

int		solution(void)
{
	find_player(&g_enemy, g_enemy.c);
	naive_force();
	return (EXIT_SUCCESS);
}
