/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   distance.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 11:53:48 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 16:50:13 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int		distance_between(int x1, int y1, int x2, int y2)
{
	int	distance;
	int	i;
	int	j;

	i = 0;
	j = 0;
	distance = 0;
	i = (x1 - x2);
	j = (y1 - y2);
	i = (i < 0) ? -i : i;
	j = (j < 0) ? -j : j;
	distance = i + j;
	return (distance);
}
