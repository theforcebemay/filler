/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_player.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/20 18:17:48 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 16:50:16 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int		find_player(t_player *player, char c)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < g_board.row)
	{
		while (j < g_board.col)
		{
			if (g_board.board[i][j] == c)
			{
				player->row = i;
				player->col = j;
				return (EXIT_SUCCESS);
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (EXIT_FAILURE + find_player(player, (ft_toupper(player->c))));
}
