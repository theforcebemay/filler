/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_info.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 11:29:55 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/28 16:25:20 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

/*
**	Copying from stdin line into 2d matrix
**	DEALLO GLOBAL IF EOGAME
*/

int		piece_into(char *line_content)
{
	static int	i;

	ft_strncpy(g_piece.piece[i++], line_content, g_piece.col);
	if (i >= g_piece.row)
		solution();
	i = i % g_piece.row;
	return (EXIT_SUCCESS);
}

/*
**	Copying from stdin line into 2d matrix
**	NO MALLOC OR FREE USED
*/

int		board_into(char *line_content)
{
	static int	i;

	while ((line_content[0] == ' ') || (ft_isdigit(line_content[0])))
		line_content++;
	ft_strncpy(g_board.board[i++], line_content, g_board.col);
	i = i % g_board.row;
	return (EXIT_SUCCESS);
}

int		p_one_or_p_two(char *line_content)
{
	if (ft_strstr(line_content, "p1"))
	{
		if (ft_strstr(line_content, "./filler"))
		{
			g_me.p = 1;
			g_me.c = 'o';
			g_enemy.p = 2;
			g_enemy.c = 'x';
		}
	}
	if (ft_strstr(line_content, "p2"))
	{
		if (ft_strstr(line_content, "./filler"))
		{
			g_me.p = 2;
			g_me.c = 'x';
			g_enemy.p = 1;
			g_enemy.c = 'o';
		}
	}
	return (EXIT_SUCCESS);
}

int		detect_line(char *line_content)
{
	if (line_content[0] == '#')
		;
	else if (ft_strstr(line_content, "$$$"))
		p_one_or_p_two(line_content);
	else if (ft_strstr(line_content, "Plateau"))
		board_width(line_content);
	else if (ft_isdigit(line_content[0]))
		board_into(line_content);
	else if (ft_strstr(line_content, "Piece"))
		piece_width(line_content);
	else if (line_content[0] == '*' || line_content[0] == '.')
		piece_into(line_content);
	else if (ft_strstr(line_content, "=="))
		deallo_global(line_content);
	return (EXIT_SUCCESS);
}

/*
**	Here programm ends! Deallocating!
*/

int		deallo_global(char *line_content)
{
	if (g_piece.piece)
		deallomatrix(g_piece.piece, g_piece.row);
	if (g_board.board)
		deallomatrix(g_board.board, g_board.row);
	g_board.board = NULL;
	g_piece.piece = NULL;
	write(1, "0 0\n", 4);
	free(line_content);
	exit(0);
}
