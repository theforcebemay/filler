/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   howto_set.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/21 18:08:09 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 17:11:24 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

/*
**	Iterates through 2d array and detects ij location
**	of player
*/

int		can_i_be_placed(int row, int col)
{
	int	i;
	int	j;
	int	count;

	RESET3(i, j, count);
	while (i < g_piece.row)
	{
		while (j < g_piece.col)
		{
			if (g_piece.piece[i][j] == '*' &&
					(g_board.board[row + i][col + j] == ft_toupper(g_me.c)
					|| g_board.board[row + i][col + j] == g_me.c))
				count++;
			if (g_board.board[row + i][col + j] == g_enemy.c
					|| g_board.board[row + i][col + j] == ft_toupper(g_enemy.c))
				return (EXIT_FAILURE);
			j++;
		}
		j = 0;
		i++;
	}
	return (count == 1) ? EXIT_SUCCESS : EXIT_FAILURE;
}
