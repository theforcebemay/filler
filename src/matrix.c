/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/27 19:54:43 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/27 19:54:49 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

int		deallomatrix(char **mx, int row)
{
	while (row >= 0)
		free(mx[row--]);
	free(mx);
	return (EXIT_SUCCESS);
}

int		makevismatrix(char **matrix, int row, int col)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < row)
	{
		while (j < col)
		{
			matrix[i][j] = 'x';
			j++;
		}
		j = 0;
		i++;
	}
	return (EXIT_SUCCESS);
}

int		printmatrix(char **matrix)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	if (!matrix || !matrix[i])
		return (EXIT_FAILURE);
	while (matrix[i])
	{
		while (matrix[i][j])
		{
			write(1, &matrix[i][j], 1);
			j++;
		}
		write(1, "\n", 1);
		j = 0;
		i++;
	}
	return (EXIT_SUCCESS);
}

char	**allomatrix(int row, int col)
{
	char	**matrix;
	int		i;

	i = 0;
	matrix = (char **)malloc(sizeof(char *) * (row + 1));
	if (!matrix)
		perror("No memory?\n");
	matrix[row] = NULL;
	while (i < row)
	{
		matrix[i] = (char *)malloc(sizeof(char) * (col + 1));
		if (!matrix[i])
			perror("No memory?\n");
		matrix[i++][col] = '\0';
	}
	return (matrix);
}

int		test_matrix(int row, int col)
{
	char **mx;

	mx = allomatrix(row, col);
	makevismatrix(mx, row, col);
	printmatrix(mx);
	deallomatrix(mx, row);
	return (0);
}
