/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readline.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bsemchuk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/10 17:55:34 by bsemchuk          #+#    #+#             */
/*   Updated: 2017/06/28 16:28:03 by bsemchuk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/filler.h"

t_player	g_me = {0, 0, 0, 0};
t_player	g_enemy = {0, 0, 0, 0};
t_board		g_board = {0, 0, 0};
t_piece		g_piece = {0, 0, 0};

int			feedback(int i, int j)
{
	ft_putnbr(i);
	ft_putchar(' ');
	ft_putnbr(j);
	ft_putchar('\n');
	return (EXIT_SUCCESS);
}

int			read_and_detect_loop(void)
{
	char	*line_content;

	while (EXIT_FAILURE)
	{
		line_content = NULL;
		line_content = get_next_line_stdin();
		if (!line_content)
			return (EXIT_FAILURE);
		detect_line(line_content);
		free(line_content);
	}
	return (EXIT_SUCCESS);
}

int			main(void)
{
	read_and_detect_loop();
	return (EXIT_SUCCESS);
}
